import java.util.Scanner;

public class cowbull {
	public static void main(String[] args) {
		int number, tmp;
		int[] digits = new int[4];
		do {
			number = (int) (Math.floor(Math.random() * (9999 - 1000)) + 1000);
			tmp = number;
			digits[3] = tmp % 10;
			tmp /= 10;
			digits[2] = tmp % 10;
			tmp /= 10;
			digits[1] = tmp % 10;
			tmp /= 10;
			digits[0] = tmp % 10;
		} while (digits[0] == digits[1] || digits[0] == digits[2] || digits[0] == digits[3] || digits[1] == digits[2]
				|| digits[1] == digits[3] || digits[2] == digits[3]);
		System.out.println(number);
		Scanner s = new Scanner(System.in);
		int curr;
		do {
			System.out.print("Enter number: ");
			curr = s.nextInt();
			while (curr < 1000 || curr > 9999) {
				System.out.print("Invalid number! Enter number: ");
				curr = s.nextInt();
			}
			int cows = 0;
			int bulls = 0;
			int[] digits2 = new int[4];
			tmp = curr;
			digits2[3] = tmp % 10;
			tmp /= 10;
			digits2[2] = tmp % 10;
			tmp /= 10;
			digits2[1] = tmp % 10;
			tmp /= 10;
			digits2[0] = tmp % 10;
			for (int i = 0; i < 4; i++) {
				if (digits[i] == digits2[i]) {
					bulls++;
				}
			}
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					if (i != j && digits2[j] == digits[i]) {
						cows++;
					}
				}
			}
			System.out.println("Cows: " + cows + "\nBulls: " + bulls);
		} while (curr != number);
		System.out.println("You win!");
		s.close();
	}
}
