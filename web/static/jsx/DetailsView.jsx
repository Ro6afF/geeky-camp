class DetailsView extends React.Component {
  constructor(props) {
    super()
    this.props = props
  }

  render() {
    return (
      <div>
        <label>ID</label><br />
        <input disabled type="text" value={this.props.record.id} class="form-control form-control-disabled" />
        <br />
        <label>First Name</label><br />
        <input class="form-control" type="text" value={this.props.record.first_name} onChange={event => this.props.actions.updateValue('first_name', event)} />
        <br />
        <label>Last Name</label><br />
        <input class="form-control" type="text" value={this.props.record.last_name} onChange={event => this.props.actions.updateValue('last_name', event)} /><br />
        <label>Shirt Size</label><br />
        <select class="form-control" type="text" value={this.props.record.shirt_size} onChange={event => this.props.actions.updateValue('shirt_size', event)} >
          <option>XXS</option>
          <option>XS</option>
          <option>S</option>
          <option>M</option>
          <option>L</option>
          <option>XL</option>
          <option>XXL</option>
        </select><br />
        <label>Shirt Colour</label><br />
        <input class="form-control" type="text" value={this.props.record.shirt_colour} onChange={event => this.props.actions.updateValue('shirt_colour', event)} /><br />
        <button class="btn btn-success" onClick={event => this.props.actions.save(this.props.record)}>Save</button>
      </div>
    )
  }
}