class App extends React.Component {
  constructor() {
    super()

    this.idSequence = 0
    this.state = {
      records: [],
      showDetails: false,
      currentRecord: {}
    }

    var xhttp = new XMLHttpRequest();
    var tmp;
    xhttp.onreadystatechange = function () {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
        tmp = JSON.parse(xhttp.responseText);
      }
    }
    xhttp.open("GET", "/get", false);
    xhttp.send();
    this.state.records = tmp;

    this.actions = {
      master: {
        create: this.create.bind(this),
        edit: this.edit.bind(this),
        delete: this.delete.bind(this)
      },
      details: {
        updateValue: this.updateValue.bind(this),
        save: this.save.bind(this),
        cancel: this.cancel.bind(this)
      }
    };
  }

  create() {
    var xhttp = new XMLHttpRequest();
    var tmp;
    xhttp.onreadystatechange = function () {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
        tmp = JSON.parse(xhttp.responseText);
      }
    }
    xhttp.open("POST", "/create", false);
    xhttp.send();
    var record = tmp[0];
    this.setState({ currentRecord: record, showDetails: true })
  }

  edit(record) {
    let copy = Object.assign({}, record)
    this.setState({ currentRecord: copy, showDetails: true })
  }

  delete(record) {
    let records = this.removeFromRecords(this.state.records, record)
    var xhttp = new XMLHttpRequest();
    xhttp.open("DELETE", "/delete?id=" + record.id, true);
    xhttp.send();
    this.setState({ records: records })
  }

  updateValue(property, event) {
    let record = this.state.currentRecord
    record[property] = event.target.value
    this.setState({ currentRecord: record })
  }

  save(record) {
    let records = this.removeFromRecords(this.state.records, record)
    records.push(record)
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/edit?first_name=" + record.first_name + "&last_name=" + record.last_name + "&shirt_size=" + record.shirt_size + "&shirt_colour=" + record.shirt_colour + "&id=" + record.id, true);
    xhttp.send();
    this.setState({ records: records })
    this.cancel()
  }

  cancel() {
    this.setState({ currentRecord: this.cleanRecord(), showDetails: false })
  }

  cleanRecord() {
    return { id: '', first_name: '', last_name: '' }
  }

  removeFromRecords(records, record) {
    return this.state.records.filter(currentRecord => currentRecord.id !== record.id)
  }

  render() {
    return (
      <div class="container">
        <MasterView records={this.state.records} actions={this.actions.master} />
        {this.state.showDetails && <DetailsView record={this.state.currentRecord} actions={this.actions.details} />}
      </div>
    )
  }
}