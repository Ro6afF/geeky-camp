class MasterView extends React.Component {
  constructor(props) {
    super()
    this.props = props
  }

  render() {
    return (
      <div>
        <table class="table table-striped table-hover table-bordered table-hover">
          <thead>
            <tr>
              <th>ID</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Shirt Size</th>
              <th>Shirt Colour</th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.records.map(record => {
                return (
                  <tr>
                    <td>{record.id}</td>
                    <td>{record.first_name}</td>
                    <td>{record.last_name}</td>
                    <td>{record.shirt_size}</td>
                    <td>{record.shirt_colour}</td>
                    <td><button class="btn btn-primary" onClick={() => this.props.actions.edit(record)}>Edit</button></td>
                    <td><button class="btn btn-danger" onClick={() => this.props.actions.delete(record)}>Delete</button></td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>
        <button class="btn btn-success" onClick={() => this.props.actions.create()}>New</button>
      </div>
    )
  }
}