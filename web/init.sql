drop database if exists shirts;
create database shirts;
use shirts;

create table records(
    id integer not null auto_increment, primary key(id),
    first_name varchar(255) not null,
    last_name varchar(255) not null,
    shirt_colour varchar(255) not null,
    shirt_size varchar(255) not null 
);