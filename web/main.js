var express = require('express');
var path = require('path');
var mysql = require('mysql');
var body_parser = require('body-parser');

var connection = mysql.createConnection({
	host: process.env.host,
	user: process.env.username,
	password: process.env.pass,
	database: process.env.db
});
connection.connect();

var app = express();
var port = 3000;

app.use(body_parser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'static')));

app.get('/get', function (req, res) {
	connection.query('select * from records', function (err, resu) {
		if (err) {
			console.log(err);
			res.status(500);
		}
		res.send(resu);
	});
});

app.delete('/delete', function (req, res) {
	connection.query('delete from records where id=?', [req.query.id], function (err, resu) {
		if (err) {
			console.log(err);
			res.status(500);
		}
		res.send(resu);
	});
});

app.post('/create', function (req, res) {
	connection.query("insert into records values (null, '', '', '', 'XXS')", function (err, resu) {
		if (err) {
			console.log(err);
			res.status(500);
			res.send();
		} else {
			connection.query('select * from records order by id desc limit 1', function (eerr, ress) {
				if (err) {
					console.log(err);
					res.status(500);
					res.send();
				}
				res.json(ress);
			})
		}
	})
});

app.post('/edit', function (req, res) {
	connection.query('update records set first_name = ? , last_name = ? , shirt_size = ? , shirt_colour = ? where id = ?', [req.query.first_name, req.query.last_name, req.query.shirt_size, req.query.shirt_colour, req.query.id], function (err, resu) {
		if (err) {
			console.log(err);
			res.status(500);
		}
		res.send();
	});
});

app.listen(port, function () {
	console.log('You may now request the application: http://localhost:%d/', port);
});