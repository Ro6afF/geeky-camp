import java.io.IOException;

import filesystem.*;

public class FS {
	public static void main(String[] args) throws IOException {
		FileSystem.createDirectory(FileSystem.ROOT, "pesho");
		FileSystem.createDirectory(FileSystem.ROOT, "goho");
		FileSystem.createDirectory("pesho", "franz");
		FileSystem.createDirectory("pesho/franz", "hans");
		FileSystem.createFile("pesho", "asd");
		FileSystem.createFile("pesho/franz", "asd");
		FileSystem.printTree(FileSystem.ROOT);
		FileSystem.writeFile("pesho/franz/hans", "asd", "ProstoPrase2000");
		System.out.println(FileSystem.readFile("pesho/franz/hans", "asd"));
		FileSystem.delete("pesho/franz/hans", "asd");
		FileSystem.printTree(FileSystem.ROOT);
		FileSystem.recursiveDelete(FileSystem.ROOT, "pesho");
		FileSystem.printTree(FileSystem.ROOT);
		FileSystem.rename(FileSystem.ROOT, "goho", "moho");
		FileSystem.printTree(FileSystem.ROOT);
	}
}