package filesystem;

public enum OpenType {
	ReadOnly, ReadWrite
}
