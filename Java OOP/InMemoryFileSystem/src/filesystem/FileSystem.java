package filesystem;

import java.io.IOException;

public class FileSystem {
	public static final String ROOT = "";
	public static Directory root = new Directory("", ROOT);

	public static void printTree(String path) {
		String[] tokens = path.split("/");
		int curr = 0;
		Directory current = root;
		if (tokens.length > 0) {
			for (int i = 0; i < current.content.size(); i++) {
				if (current.content.get(i).getName() == tokens[curr]) {
					current = (Directory) current.content.get(i);
					i = 0;
					curr++;
					if (curr == tokens.length) {
						break;
					}
				}
			}
		}
		printTree(current, 0);
	}

	private static void printTree(Node dir, int depth) {
		for (int i = 0; i < depth; i++) {
			System.out.print('\t');
		}
		System.out.println(dir.getFullPath());
		if (dir instanceof Directory) {
			for (Node n : ((Directory) dir).content) {
				printTree(n, depth + 1);
			}
		}
	}

	public static Node find(String path) throws IOException {
		if (path == ROOT) {
			return root;
		}
		String[] tokens = path.split("/");
		Directory current = root;
		for (String token : tokens) {
			for (int i = 0; i < current.content.size(); i++) {
				if (current.content.get(i).getName().equals(token)) {
					if (token == tokens[tokens.length - 1]) {
						return current.content.get(i);
					}
					current = (Directory) current.content.get(i);
					break;
				}
			}
		}
		throw new IOException("Path not found!");
	}

	public static void createFile(String path, String name) throws IOException {
		((Directory) find(path)).addFile(name);
	}

	public static void createDirectory(String path, String name) throws IOException {
		((Directory) find(path)).createSubDirectory(name);
	}

	public static String readFile(String path, String name) throws IOException {
		File file = (File) find(path + '/' + name);
		file.open();
		return file.read();
	}

	public static void writeFile(String path, String name, String content) throws IOException {
		File file;
		try {
			find(path + '/' + name);
		} catch (IOException e) {
			if (e.getMessage() == "Path not found!") {
				createFile(path, name);
			}
		} finally {
			file = (File) find(path + '/' + name); 
		}
		file.open(OpenType.ReadWrite);
		file.write(content);
	}

	public static void delete(String path, String name) throws IOException {
		Directory parent = (Directory) find(path);
		parent.delete(name);
	}

	public static void recursiveDelete(String path, String name) throws IOException {
		Directory parent = (Directory) find(path);
		parent.deleteRecursively(name);
	}

	public static void rename(String path, String name, String new_name) throws IOException {
		find(path + '/' + name).rename(new_name);
	}
}
