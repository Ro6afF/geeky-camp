package filesystem;

import java.io.IOException;

public class File implements Node {
	private String name;
	private String path;
	private String data;
	private boolean isOpened;
	private boolean canWrite;

	public File(String name, String path) {
		this.name = name;
		this.path = path;
		isOpened = false;
		canWrite = false;
		data = "";
	}

	public String getFullPath() {
		return path + '/' + name;
	}

	public void open() throws IOException {
		if (isOpened) {
			throw new IOException("File already opened");
		}
		isOpened = true;
	}

	public void open(OpenType t) throws IOException {
		open();
		canWrite = t == OpenType.ReadWrite;
	}

	public void close() {
		isOpened = false;
	}

	public String read() throws IOException {
		if (!isOpened) {
			throw new IOException("Not opened");
		}
		close();
		return this.data;
	}

	public void write(String data) throws IOException {
		if (!isOpened || !canWrite) {
			throw new IOException("Not opened int ReadWrite mode!");
		}
		for (char ch : data.toCharArray()) {
			this.data += ch;
		}
		close();
	}

	public void delete() throws IOException {
		if (isOpened) {
			throw new IOException("File opened");
		}
	}

	public void rename(String name) throws IOException {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public String getRootPath() {
		return path;
	}
}
