package filesystem;

import java.io.IOException;

public interface Node {
	public void delete() throws IOException;

	public String getFullPath();

	public String getName();

	public String getRootPath();

	public void rename(String name) throws IOException;
}
