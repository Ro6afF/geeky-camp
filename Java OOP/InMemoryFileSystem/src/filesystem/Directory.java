package filesystem;

import java.io.IOException;
import java.util.ArrayList;

public class Directory implements Node {
	ArrayList<Node> content;
	private String name;
	private String path;

	public Directory(String name, String path) {
		content = new ArrayList<Node>();
		this.name = name;
		this.path = path;
	}

	public String getFullPath() {
		return path + '/' + name;
	}

	public void addFile(String name) throws IOException {
		for (Node x : content) {
			if (x.getName() == name) {
				throw new IOException("Node with same name already exists!");
			}
		}
		content.add(new File(name, this.getFullPath()));
	}

	public void createSubDirectory(String name) throws IOException {
		for (Node x : content) {
			if (x.getName() == name) {
				throw new IOException("Node with same name already exists!");
			}
		}
		content.add(new Directory(name, this.getFullPath()));
	}

	public void addFile(String name, String contnent) throws IOException {
		addFile(name);
		File current = (File) this.content.get(content.size() - 1);
		current.open(OpenType.ReadWrite);
		current.write(contnent);
		current.close();
	}

	public void delete() throws IOException {
		if (content.size() != 0) {
			throw new IOException("Directory non empty");
		}
	}

	public void delete(String name) throws IOException {
		for (int i = 0; i < content.size(); i++) {
			if (content.get(i).getName().equals(name)) {
				content.get(i).delete();
				content.remove(i);
			}
		}
	}

	public void deleteRecursively() throws IOException {
		for (Node x : content) {
			if (x instanceof Directory) {
				((Directory) x).deleteRecursively();
			} else {
				x.delete();
			}
		}
		content.clear();
	}

	public void deleteRecursively(String name) throws IOException {
		for (int i = 0; i < content.size(); i++) {
			if (content.get(i).getName().equals(name)) {
				((Directory) content.get(i)).deleteRecursively();
				content.remove(i);
			}
		}
	}

	public void rename(String name) throws IOException {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public String getRootPath() {
		return path;
	}
}
