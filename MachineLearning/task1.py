import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.cross_validation import train_test_split
import math

data = np.array(pd.read_csv("./battery-data.csv", header=None))
data.sort(axis=0)
data = pd.DataFrame(data)
X = np.array(data[data.columns[0]]).reshape(data.shape[0], 1)
Y = np.array(data[data.columns[1]]).reshape(data.shape[0], 1)

x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=0)

linear_regression = linear_model.LinearRegression()
linear_regression.fit(x_train, y_train)
y_pred = linear_regression.predict(x_test)

print("Mean Squared Error: %.2f" % mean_squared_error(y_test, y_pred))
print("Variance: %.2f" % r2_score(y_test, y_pred))

plt.scatter(X, Y)
plt.scatter(x_test, y_pred, c="red")
plt.xlabel("Charge time")
plt.ylabel("Discharge time")
plt.show()
