#!/bin/bash

while read -r line
do
	eval "$line"
done < "$3"

while read -r line
do
	prevl=""
	while [ "$prevl" != "$line" ]
	do
		prevl=$line
		line=$(eval echo "$(echo "$line" | sed -r 's/@([^@]+)@/${\1}/g')")
	done
	echo $line
done < "$1" > "$2"
