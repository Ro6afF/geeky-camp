package mysql;

import java.sql.*;

public class UnivertisyDB {
	public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	public static final String DB_URL = "jdbc:mysql://localhost/hw";
	public static final String USERNAME = "asd"; // I know it's stupid
	public static final String PASSWORD = "asd";
	public static Connection connection;

	public static void connect() throws ClassNotFoundException, SQLException {
		Class.forName(JDBC_DRIVER);
		connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
	}

	public static void addFaculty(String name) throws SQLException {
		PreparedStatement s = connection.prepareStatement("INSERT INTO FACULTY VALUES (NULL, ?);");
		s.setString(1, name);
		s.execute();
		s.close();
	}

	public static void addStudent(String first_name, String second_name, int faculty_id) throws SQLException {
		PreparedStatement s = connection.prepareStatement("INSERT INTO STUDENT VALUES (NULL, ?, ?, ?);");
		s.setString(1, first_name);
		s.setString(2, second_name);
		s.setInt(3, faculty_id);
		s.execute();
		s.close();
	}

	public static void addCource(String name, String description, int credits) throws SQLException {
		PreparedStatement s = connection.prepareStatement("INSERT INTO COURCE VALUES (NULL, ?, ?, ?);");
		s.setString(1, name);
		s.setString(2, description);
		s.setInt(3, credits);
		s.execute();
		s.close();
	}

	public static void enrollStudentForCource(int student_number, int cource_id) throws SQLException {
		PreparedStatement s = connection.prepareStatement("INSERT INTO COURCE_STUDENT VALUES (NULL, ?, ?);");
		s.setInt(1, student_number);
		s.setInt(2, cource_id);
		s.execute();
		s.close();
	}

	public static void kickStudentFromCource(int student_number, int cource_id) throws SQLException {
		PreparedStatement s = connection.prepareStatement(
				"DELETE FROM COURCE_STUDENT WHERE COURCE_STUDENT.STUDENT_FC = ? AND COURCE_STUDENT.COURCE_ID = ?;");
		s.setInt(1, student_number);
		s.setInt(2, cource_id);
		s.execute();
		s.close();
	}

	public static void deleteStudent(int student_number) throws SQLException {
		PreparedStatement s = connection.prepareStatement(
				"DELETE COURCE_STUDENT FROM COURCE_STUDENT INNER JOIN STUDENT ON COURCE_STUDENT.STUDENT_FC = STUDENT.FACULTY_NUMBER WHERE STUDENT.FACULTY_NUMBER = ?;");
		PreparedStatement s1 = connection.prepareStatement("DELETE FROM STUDENT WHERE STUDENT.FACULTY_NUMBER = ?;");
		s.setInt(1, student_number);
		s1.setInt(1, student_number);
		s.execute();
		s1.execute();
		s.close();
		s1.close();
	}

	public static void deleteFaculty(int id) throws SQLException {
		PreparedStatement s = connection.prepareStatement(
				"DELETE COURCE_STUDENT FROM COURCE_STUDENT INNER JOIN STUDENT ON COURCE_STUDENT.STUDENT_FC = STUDENT.FACULTY_NUMBER WHERE STUDENT.FACULTY_ID = ?;");
		PreparedStatement s1 = connection.prepareStatement("DELETE FROM STUDENT WHERE STUDENT.FACULTY_ID = ?;");
		PreparedStatement s2 = connection.prepareStatement("DELETE FROM FACULTY WHERE FACULTY.ID = ?;");
		s.setInt(1, id);
		s1.setInt(1, id);
		s2.setInt(1, id);
		s.execute();
		s1.execute();
		s2.execute();
		s.close();
		s1.close();
		s2.close();
	}

	public static void deleteCource(int i) throws SQLException {
		PreparedStatement s = connection.prepareStatement(
				"DELETE COURCE_STUDENT FROM COURCE_STUDENT INNER JOIN COURCE ON COURCE.ID = COURCE_STUDENT.COURCE_ID WHERE COURCE.ID = ?;");
		PreparedStatement s1 = connection.prepareStatement("DELETE FROM COURCE WHERE COURCE.ID = ?;");
		s.setInt(1, i);
		s1.setInt(1, i);
		s.execute();
		s1.execute();
		s.close();
		s1.close();
	}

	public static void printStudents() throws SQLException {
		PreparedStatement s = connection.prepareStatement("SELECT * FROM STUDENT");
		ResultSet rs = s.executeQuery();
		while(rs.next()) {
			int fn = rs.getInt("FACULTY_NUMBER");
			String f1 = rs.getString("FIRST_NAME");
			String f2 = rs.getString("LAST_NAME");
			int fid = rs.getInt("FACULTY_ID");
			System.out.println(fn + " " + f1 + " " + f2 + " " + fid);
		}
		s.close();
	}
	
	public static void printCources() throws SQLException {
		PreparedStatement s = connection.prepareStatement("SELECT * FROM COURCE");
		ResultSet rs = s.executeQuery();
		while(rs.next()) {
			int fn = rs.getInt("ID");
			String f1 = rs.getString("NAME");
			String f2 = rs.getString("DESCRIPTION");
			int fid = rs.getInt("CREDITS");
			System.out.println(fn + " " + f1 + " " + f2 + " " + fid);
		}
		s.close();
	}
	
	public static void printFaculties() throws SQLException {
		PreparedStatement s = connection.prepareStatement("SELECT * FROM FACULTY;");
		ResultSet rs = s.executeQuery();
		while(rs.next()) {
			int fn = rs.getInt("ID");
			String f1 = rs.getString("NAME");
			System.out.println(fn + " " + f1);
		}
		s.close();
	}
	
	public static void printFacultyStudents(int faculty_id) throws SQLException {
		PreparedStatement s = connection.prepareStatement("SELECT * FROM STUDENT WHERE STUDENT.FACULTY_ID = ?;");
		s.setInt(1, faculty_id);
		ResultSet rs = s.executeQuery();
		while(rs.next()) {
			int fn = rs.getInt("FACULTY_NUMBER");
			String f1 = rs.getString("FIRST_NAME");
			String f2 = rs.getString("LAST_NAME");
			String fi = rs.getString("FACULTY_ID");
			System.out.println(fn + " " + f1 + " " + f2 + " " + fi);
		}
		s.close();
	}
	
	public static void printStudentCources(int faculty_number) throws SQLException {
		PreparedStatement s = connection.prepareStatement("SELECT * FROM COURCE_STUDENT INNER JOIN COURCE ON COURCE.ID = COURCE_STUDENT.COURCE_ID WHERE COURCE_STUDENT.STUDENT_FC = ?;");
		s.setInt(1, faculty_number);
		ResultSet rs = s.executeQuery();
		while(rs.next()) {
			int fn = rs.getInt("COURCE_ID");
			String fi = rs.getString("NAME");
			System.out.println(fn + " " + fi);
		}
		s.close();
	}
	
	static void disconnect() throws SQLException {
		connection.close();
	}


}
