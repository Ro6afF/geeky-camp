package mysql;

import java.sql.SQLException;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		UnivertisyDB.connect();
		UnivertisyDB.addFaculty("FMI");
		UnivertisyDB.printFaculties();
		UnivertisyDB.addStudent("Pesho", "Goshov", 1);
		UnivertisyDB.addStudent("Gosho", "Peshov", 1);
		UnivertisyDB.addStudent("ASD", "SAD", 1);
		UnivertisyDB.printStudents();
		UnivertisyDB.deleteStudent(3);
		UnivertisyDB.printStudents();
		UnivertisyDB.printFacultyStudents(1);
		UnivertisyDB.addCource("asd", "asdf", 42);
		UnivertisyDB.addCource("sdf", "sdfg", 24);
		UnivertisyDB.addCource("blq", "lqb", 24);
		UnivertisyDB.printCources();
		UnivertisyDB.deleteCource(3);
		UnivertisyDB.printCources();
		UnivertisyDB.enrollStudentForCource(1, 1);
		UnivertisyDB.enrollStudentForCource(1, 2);
		UnivertisyDB.printStudentCources(1);
		UnivertisyDB.kickStudentFromCource(2, 1);
		UnivertisyDB.deleteFaculty(1);
		UnivertisyDB.printFaculties();
	}

}
