var DDNS = artifacts.require("DDNS");

contract('DDNS', function (accounts) {
  it("working case", function () {
    return DDNS.deployed().then(async function (instance) {
      await instance.mint(accounts[0], 42);
      await instance.mint(accounts[1], 42);
      await instance.buyDomain("pesho.org", "org.pesho", { from: accounts[0] });
      await instance.buyDomain("gosho.org", "org.gosho", { from: accounts[1] });
      instance.getIP.call("pesho.org").then(function (domain) {
        assert.equal(domain.valueOf(), "org.pesho")
      });
      instance.getIP.call("gosho.org").then(function (domain) {
        assert.equal(domain.valueOf(), "org.gosho")
      });
      await instance.changeIP("pesho.org", "asdf", { from: accounts[0] });
      instance.getIP.call("pesho.org").then(function (domain) {
        assert.equal(domain.valueOf(), "asdf")
      });
    });
  });

  it("Not owner change IP", function () {
    return DDNS.deployed().then(async function (instance) {
      try {
        await instance.changeIP("pesho.org", "ssdfg", { from: accounts[1] });
      } catch (error) {
        return;
      }
      should.fail('Expected revert not received');
    });
  });

  it("Not owner change IP", function () {
    return DDNS.deployed().then(async function (instance) {
      try {
        await instance.changeIP("pesho.org", "ssdfg", { from: accounts[1] });
      } catch (error) {
        return;
      }
      should.fail('Expected revert not received');
    });
  });

  it("Existing record", function () {
    return DDNS.deployed().then(async function (instance) {
      try {
        await instance.buyDomain("pesho.org", "ssdfg", { from: accounts[1] });
      } catch (error) {
        return;
      }
      should.fail('Expected revert not received');
    });
  });

  it("No money", function () {
    return DDNS.deployed().then(async function (instance) {
      try {
        await instance.buyDomain("shushumiga", "ssdfg", { from: accounts[3] });
      } catch (error) {
        return;
      }
      should.fail('Expected revert not received');
    });
  });

  /*
    it("should send 2 tokens to some account", function () {
      return DDNS.deployed().then(function (instance) {
        instance.mint(accounts[0], 12, { from: accounts[0] }).then(function () {
          instance.send(accounts[1], 1, { from: accounts[0] });
          instance.balances.call(accounts[0]).then(function (balance) {
            assert.equal(balance.valueOf(), 11);
          });
          instance.balances.call(accounts[1]).then(function (balance) {
            assert.equal(balance.valueOf(), 1);
          });
        });
      });
    });
  
    it("should give domain", function () {
      return DDNS.deployed().then(function (instance) {
        instance.mint(accounts[0], 12).then(
          instance.buyDomain("pesho.org", "org.pesho", { from: accounts[0] }).then(
            instance.getIP.call("pesho.org").then(function (IP) {
              assert.equal(IP.valueOf(), "org.pesho");
            })));
      });
    });*/
});
