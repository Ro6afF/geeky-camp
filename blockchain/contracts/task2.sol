pragma solidity ^0.4.7;

import "./SafeMath.sol";

contract DDNS {
    struct domain {
        address owner;
        string ip;
    }

    uint constant price = 1;
    uint constant tokenPrice = 1e18;
    address public contract_owner;
    mapping(string => domain) map;
    uint balance = 0;
    mapping(address => uint) public balances;
    
    constructor() public {
        contract_owner = msg.sender;
    }

    function mint(address reciever, uint amount) public {
        require(msg.sender == contract_owner, "Not the minter");
        balances[reciever] = SafeMath.add(balances[reciever], amount);
    }
    
    function send(address reciever, uint amount) public {
        require(balances[msg.sender] >= amount, "Not enough tokens");
        balances[reciever] = SafeMath.add(amount, balances[reciever]);
        balances[msg.sender] = SafeMath.sub(balances[msg.sender], amount);
    }
    
    function buyDomain(string ndomain, string ip) public payable {
        require(map[ndomain].owner == 0, "Domain exists");
        require(balances[msg.sender] >= price, "Not enough tokens");
        balances[msg.sender] = SafeMath.sub(balances[msg.sender], price);
        balance = SafeMath.add(price, balance);
        map[ndomain].owner = msg.sender;
        map[ndomain].ip = ip;
    }
    
    function changeIP(string ndomain, string ip) public {
        require(map[ndomain].owner == msg.sender, "Not the owner");
        map[ndomain].ip = ip;
    }
    
    function getIP(string ndomain) public view returns(string ans) {
        ans = map[ndomain].ip;
    }
    
    function withdraw(uint amount) public {
        require(msg.sender == contract_owner, "Not the contract owner");
        require(balance >= amount, "Not enough tokens");
        balance = SafeMath.sub(balance, amount);
        balances[msg.sender] = SafeMath.add(balances[msg.sender], amount);
    }
}