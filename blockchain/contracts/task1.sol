pragma solidity ^0.4.7;

contract task1 {
    address owner;
    uint public counter;
    
    constructor() public {
        owner = msg.sender;
        counter = 0;
    }
    
    function increase () public {
        require(msg.sender == owner && counter + 1 > counter);
        counter ++;
    }
}

