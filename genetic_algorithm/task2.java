import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Collectors;

public class task2 {
	static class Creature implements Comparable<Creature> {
		public String value;
		public int fitness;

		public Creature() {
			value = "";
		}

		public int compareTo(Creature c) {
			return ((Integer) fitness).compareTo(c.fitness);
		}
	}

	static final String CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvwxyz" + "0123456789";
	static final int NUMBER_CREATURES = 100;
	static final int ELITISM = 4;
	static final double MUTATUON_CHANCE = 0.2;
	static String expected;
	static Creature[] creatures;
	static Random rng = new Random();

	static void calculateFitnesses() {
		for (int i = 0; i < NUMBER_CREATURES; i++) {
			creatures[i].fitness = 0;
			for (int j = 0; j < expected.length(); j++) {
				if (creatures[i].value.charAt(j) != expected.charAt(j)) {
					creatures[i].fitness++;
				}
			}
		}
		Arrays.sort(creatures);
	}

	static void generateNextPopulation() {
		int current = ELITISM + 1;
		for (int i = 0; i < ELITISM - 1; i++) {
			for (int j = i + 1; j < ELITISM; j++) {
				creatures[current] = new Creature();
				for (int k = 0; k < expected.length(); k++) {
					creatures[current].value += creatures[(rng.nextBoolean()) ? i : j].value.charAt(k);
				}
				current++;
			}
		}
		int top = current;
		while (current != NUMBER_CREATURES - 1) {
			for (int i = 0; i < top && current < NUMBER_CREATURES; i++) {
				creatures[current] = new Creature();
				for (int j = 0; j < expected.length(); j++) {
					creatures[current].value += (rng.nextDouble() < MUTATUON_CHANCE) ? creatures[i].value.charAt(j)
							: CHARS.charAt(rng.nextInt(CHARS.length()));

				}
				current++;
			}
		}
	}

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		expected = s.next();
		s.close();
		creatures = new Creature[NUMBER_CREATURES];
		for (int i = 0; i < NUMBER_CREATURES; i++) {
			creatures[i] = new Creature();
			creatures[i].value = rng.ints(expected.length(), 0, CHARS.length()).mapToObj(j -> "" + CHARS.charAt(j))
					.collect(Collectors.joining());
		}
		while (true) {
			calculateFitnesses();
			System.out.println(creatures[0].fitness + " " + creatures[0].value);
			if(creatures[0].fitness == 0) {
				return;
			}
			generateNextPopulation();
		}
	}
}
