package app.camp.geeky.com.geekycampapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.time.format.TextStyle;
import java.util.Map;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Activity activity;
    TextToSpeech toSpeech;
    boolean isInitialised;
    Button bCreateNote;
    Button bReadAll;
    LinearLayout lScrolls;

    public void read(String w) {
        if (isInitialised) {
            toSpeech.speak(w, TextToSpeech.QUEUE_ADD, null, null);
        } else {
            Toast.makeText(this, "Object not initialized properly!", Toast.LENGTH_SHORT).show();
        }
    }

    public void loadNotes() {
        SharedPreferences sp = getSharedPreferences("geeky-note", MODE_PRIVATE);
        Map<String, ?> notes = sp.getAll();
        lScrolls = findViewById(R.id.lScrolls);
        lScrolls.removeAllViews();
        for(String s: notes.keySet()) {
            LinearLayout l = new LinearLayout(this);
            l.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
            TextView tvT = new TextView(this);
            LinearLayout.LayoutParams tfParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            tfParams.setMargins(0, 0, 0, 10);
            tvT.setLayoutParams(tfParams);
            tvT.setTextSize(20);
            tvT.setText(s);
            tvT.setBackgroundColor(Color.rgb(224, 224, 224));
            l.addView(tvT);
            lScrolls.addView(l);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadNotes();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sp = getSharedPreferences("geeky-note", MODE_PRIVATE);
        setContentView(R.layout.main_activity);
        loadNotes();
        activity = this;
        bCreateNote = findViewById(R.id.bCreateNote);
        bReadAll = findViewById(R.id.bReadAll);
        bCreateNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity,CreateNoteActivity.class);
                startActivity(i);
            }
        });
        bReadAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = getSharedPreferences("geeky-note", MODE_PRIVATE);
                Map<String, ?> notes = sp.getAll();
                for (String t: notes.keySet()) {
                    read("Note: " + t);
                }
            }
        });
        initSpeech();
    }

    public void initSpeech() {
        isInitialised = false;
        toSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    isInitialised = true;
                }
            }
        });
    }
}
