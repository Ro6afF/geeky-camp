package app.camp.geeky.com.geekycampapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class CreateNoteActivity extends AppCompatActivity implements View.OnClickListener {

    boolean listen_content =  false;
    EditText tfTitle;
    Activity act;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        act=this;
        setContentView(R.layout.activity_create_note);
        findViewById(R.id.bAddNote).setOnClickListener(this);
        findViewById(R.id.bSpeechTitle).setOnClickListener(this);
        tfTitle = findViewById(R.id.tfTitle);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bSpeechTitle:
                listen_content = false;
                speakPressed();
                break;
            case R.id.bAddNote:
                addNote();
                break;
        }
    }

    public void addNote() {
        SharedPreferences sp = getSharedPreferences("geeky-note", MODE_PRIVATE);
        Log.i("tftitletext", tfTitle.getText().toString());
        if(!tfTitle.getText().toString().equals("")) {
            sp.edit().putString(tfTitle.getText().toString(), "sth").commit();
            this.finish();
        }
    }

    public void speakPressed() {
        Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH);

        startActivityForResult(i, 200);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 200) {
            if (resultCode == RESULT_OK && data != null) {
                ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                String sResult = result.get(0);
                tfTitle.setText(sResult, TextView.BufferType.EDITABLE);
            }
        }
    }
}
