#include <SDL2/SDL.h>
#include <iostream>
#include <vector>
#include <random>
#include <math.h>

#define WIDTH 800
#define HEIGHT 600

int dX = 10;
int dY = 0;

std::vector<SDL_Rect> tail;
std::vector<std::pair<SDL_Rect, int>> food;

SDL_Rect new_SDL_Rect(int x, int y, int w, int h)
{
	SDL_Rect ret;
	ret.x = x;
	ret.y = y;
	ret.w = w;
	ret.h = h;
	return ret;
}

int main()
{
	std::random_device rd;
	std::mt19937 rng(rd());
	std::uniform_int_distribution<int> distW(0, WIDTH - 10);
	std::uniform_int_distribution<int> distH(0, HEIGHT - 10);
	std::uniform_int_distribution<int> distM(0, 3);

    int score = 0;
	int food_count = 42;
gulag:
	food.clear();
	for(int i = 0; i < food_count; i++)
	{
		food.push_back({new_SDL_Rect(distW(rng), distH(rng), 10, 10), distM(rng)});
	}

	tail.push_back(new_SDL_Rect(10, 10, 10, 10));
	tail.push_back(new_SDL_Rect(0, 0, 10, 10));

	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Event event;

	SDL_Window *win = SDL_CreateWindow("Snake", 100, 100, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	if (win == nullptr)
	{
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (ren == nullptr)
	{
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	bool runs = true;
	bool canImove;
	bool play = false;
	while(runs)
	{
		canImove = true;
		for(size_t i = 1; i < tail.size(); i++)
		{
			if (tail.size() == 0 || (tail[i].x < tail[0].x + 10 && tail[i].x + 10 > tail[0].x 
					&& tail[i].y < tail[0].y+10 && tail[i].y + 10 > tail[0].y))
			{
				if(!play) 
					goto gulag;
				std::cout<<"You died!!!"<<std::endl;
				goto GULAG;
			}
		}
		play = true;
		for(size_t i = 0; i < food.size(); i++)
		{
			if (food[i].first.x < tail[0].x + 10 && food[i].first.x + 10 > tail[0].x 
					&& food[i].first.y < tail[0].y+10 && food[i].first.y + 10 > tail[0].y) 
			{
                if(food[i].second == 1){
                    score++;
                    std::cout << score << std::endl;
                    tail.push_back(tail[tail.size() - 1]);
                    while(food[i].first.x % 10 != 0)
                    {
                        food[i].first.x --;
                    }
                    while(food[i].first.y % 10 != 0)
                    {
                        food[i].first.y --;
                    }
                } else if(food[i].second == 2) {
					score--;
					tail.pop_back();
					if(tail.size() == 0) {
						std::cout<<"You died!!!"<<std::endl;
						goto GULAG;
					}
					std::cout << score << std::endl;
				}
                food[i] = {new_SDL_Rect(distW(rng), distH(rng), 10, 10), distM(rng)};
			}
		}
		for(size_t i = tail.size() - 1; i >= 1; i--)
		{
			tail[i] = tail[i-1];
		}
		tail[0].x += dX;
		tail[0].y += dY;
		SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
		SDL_RenderClear(ren);
		for(size_t i = 0; i < food.size(); i++)
		{
			if(food[i].second != 2)
				SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
			else
				SDL_SetRenderDrawColor(ren, 0, 0, 255, 255);
			SDL_RenderFillRect(ren, &food[i].first);
		}
		for(size_t i = 0; i < tail.size(); i++)
		{
			SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
			SDL_RenderFillRect(ren, &tail[i]);
			SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
			SDL_RenderDrawRect(ren, &tail[i]);
		}
		SDL_RenderPresent(ren);
		SDL_Delay(100);
		while(SDL_PollEvent(&event))
		{
			switch (event.type)
			{
				case SDL_QUIT:
					runs = false;
					break;
				case SDL_KEYDOWN:
					if(canImove)
					{
						switch(event.key.keysym.sym)
						{
							case SDLK_UP:
								if(dY!=10)
								{
									dX = 0;
									dY = -10;
									canImove = false;
								}
								break;
							case SDLK_DOWN:
								if(dY!= -10)
								{
									dX = 0;
									dY = 10;
									canImove = false;
								}
								break;
							case SDLK_LEFT:
								if(dX != 10)
								{
									dX = -10;
									dY = 0;
									canImove = false;
								}
								break;
							case SDLK_RIGHT:
								if(dX != -10)
								{
									dX = 10;
									dY = 0;
									canImove = false;
								}
								break;
							case SDLK_ESCAPE:
								runs = false;
								break;
						}
					}
					break;
			}
		}
	}
GULAG:
	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}